@extends('Emails.Layouts.Master')

@section('message_content')
Hola {{$attendee->first_name}},<br><br> 

Tienes una cita que agendar ya que fuiste seleccionad@ para participar en <b>{{$attendee->order->event->title}}</b>, esperamos que esta sea una gran experiencia  para tí, recuerda que: 
<br><br><b>“Todo gran proyecto nació siendo una idea y toda idea nace en estado ON”</b><br><br>
La cita es el día jueves 1 de septiembre en la ciudad de Cochabamba a las 17:30 en Calle Portales N° 359 esq. Av. Villarroel, UNIFRANZ - Sede Cochabamba
<br><br>
Te enviamos tu código QR para el ingreso.
<br><br>
Te esperamos,<br>
Saludos
@stop


